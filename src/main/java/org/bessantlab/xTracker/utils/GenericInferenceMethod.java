package org.bessantlab.xTracker.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bessantlab.xTracker.Utils;

/**
 *
 * @author Jun Fan@cranfield
 */
public class GenericInferenceMethod {

    /**
     * assign the middle data structure by the quantitation values from the lower level
     */
    private static Map<String, List<Double>> assignMiddleStructure(List<String> assayIDs,List<Map<String, Double>> lowLevelQuant) {
        Map<String, List<Double>> tmp = initializeMiddleStructure(assayIDs);
        for (Map<String, Double> quant : lowLevelQuant) {
            for (String assayID : assayIDs) {
                if (quant.containsKey(assayID)) {
                    tmp.get(assayID).add(quant.get(assayID));
//                }else{
//                    tmp.get(assayID).add(0d);
                }
            }
        }
        return tmp;
    }
    /**
     * initialize the middle data structure, keys are assay IDs and values are the list of related values
     */
    private static Map<String, List<Double>> initializeMiddleStructure(List<String> assayIDs) {
        Map<String,List<Double>> tmp = new HashMap<>();
        for(String assayID:assayIDs){
            ArrayList<Double> value = new ArrayList<Double>();
            tmp.put(assayID, value);
        }
        return tmp;
    }

    /**
     * initialize the ret value
     */
    private static Map<String,Double> initializeRet(List<String> names){
        Map<String,Double> ret = new HashMap<>();
        for(String name:names){
            ret.put(name, 0d);
        }
        return ret;
    }
    /**
     * The sum method
     * @param lowLevelQuant
     * @param assayIDs
     * @return
     */
    public static Map<String,Double> sum(List<Map<String, Double>> lowLevelQuant, List<String> assayIDs) {
        Map<String,Double> ret = initializeRet(assayIDs);
        Map<String, List<Double>> tmp = assignMiddleStructure(assayIDs, lowLevelQuant);
        for(String assayID:assayIDs){
            List<Double> list = tmp.get(assayID);
            ret.put(assayID, Utils.sum(list));
        }
        return ret;
    }
    /**
     * The median method
     * @param lowLevelQuant
     * @param assayIDs
     * @return
     */
    public static Map<String,Double> median(List<Map<String, Double>> lowLevelQuant, List<String> assayIDs){
        Map<String,Double> ret = initializeRet(assayIDs);
        Map<String, List<Double>> tmp = assignMiddleStructure(assayIDs, lowLevelQuant);
        //assayID, quantitation
        for(String assayID:assayIDs){
            List<Double> list = tmp.get(assayID);
            ret.put(assayID, Utils.median(list));
        }
        return ret;
    }
    /**
     * The mean method
     * @param lowLevelQuant
     * @param assayIDs
     * @return
     */
    public static Map<String,Double> mean(List<Map<String, Double>> lowLevelQuant, List<String> assayIDs){
        Map<String,Double> ret = initializeRet(assayIDs);
        Map<String, List<Double>> tmp = assignMiddleStructure(assayIDs, lowLevelQuant);
        for(String assayID:assayIDs){
            List<Double> list = tmp.get(assayID);
            if(list==null || list.size()==0){
                ret.put(assayID, null);
            }else{
                ret.put(assayID, Utils.sum(list)/list.size());
            }
        }
        return ret;
    }
    /**
     * Calculate the weighted mean
     * @param lowLevelQuant
     * @param assayIDs
     * @param count
     * @return
     */
    public static Map<String,Double> weightedAverage(List<Map<String, Double>> lowLevelQuant, List<String> assayIDs, List<Integer> count){
        Map<String,Double> ret = initializeRet(assayIDs);
        Map<String, List<Double>> tmp = initializeMiddleStructure(assayIDs);
        for (int i = 0; i < lowLevelQuant.size(); i++) {
            Map<String, Double> quant = lowLevelQuant.get(i);
            for(String assayID:assayIDs){
                if(quant.containsKey(assayID)){
                    if(quant.get(assayID)==null){
                        tmp.get(assayID).add(null);
                    }else{
                        tmp.get(assayID).add(quant.get(assayID)*count.get(i));
                    }
                }
            }
        }

        int totalCount = 0;
        for(int abc:count){
            totalCount += abc;
        }

        for(String assayID:assayIDs){
            List<Double> list = tmp.get(assayID);
            list = Utils.filter(list);
            int len = list.size();
            if(len == 0){
                ret.put(assayID, null);
            }else{
                double sum = 0;
                for (Double value:list) {
                    sum += value;
                }
                ret.put(assayID, sum/totalCount);
            }
        }
        return ret;
    }
    /**
     * The intensity weighted average method for calculating protein ratios from peptide ratios
     * @param ratioValues
     * @param ratioIDs
     * @return
     */
    public static Map<String,Double> intensityWeightedAverage(List<Map<String, Double>> ratioValues, List<String> ratioIDs, List<Double> weights){
        Map<String,Double> ret = initializeRet(ratioIDs);
        Map<String, List<Double>> tmp = assignMiddleStructure(ratioIDs, ratioValues);

        for(String ratioID:ratioIDs) {
            List<Double> ratios  = tmp.get(ratioID);

            double numerator = 0;
            double denominator = 0;

            // ratios and weights are ordered, the same indices refer to the same peptide
            for (int i=0; i<ratios.size(); i++) {
                numerator += ratios.get(i)*weights.get(i);
                denominator += weights.get(i);
            }

            if (denominator > 0) {
                ret.put(ratioID, numerator/denominator);
            } else {
                ret.put(ratioID, 0.0);
            }
        }

        return ret;
    }
}