package org.bessantlab.xTracker.data;

import java.util.ArrayList;
import uk.ac.liv.pgb.jmzqml.model.mzqml.Feature;

/**
 *
 * @author Jun Fan
 */
public class xFeature extends QuantitationLevel{
    /**
     * the charge value
     */
    private int charge;
    /**
     * The ms run id this feature belongs to
     */
    private String msrunID;
    /**
     * Peptide ID, normally in the form of peptideSeq_modificationString
     * For generating feature ID only, not a reference back to peptide
     */
    private String peptideID;
    private ArrayList<Identification> identifications;
    
    public xFeature(String msrun,String peptideID, int charge){
        this.peptideID = peptideID;
        this.charge = charge;
        msrunID = msrun;
        identifications = new ArrayList<Identification>();
    }
    /**
     * Get the charge
     * @return The charge.
     */
    public int getCharge() {
        return charge;
    }
    /**
     * Get the feature id
     * @return The feature ID.
     */
    public String getId() {
        StringBuilder sb = new StringBuilder();
        sb.append(msrunID);
        sb.append("-");
        sb.append(peptideID);
        sb.append("-");
        sb.append(charge);
        return sb.toString();
    }
    /**
     * Get all identification under the feature
     * @return The identifications.
     */
    public ArrayList<Identification> getIdentifications() {
        return identifications;
    }
    /**
     * Add one identification
     * @param identification The identification.
     */
    public void addIdentification(Identification identification){
        identifications.add(identification);
    }
    /**
     * Calculate the average m/z of the region which the feature represents
     * @return The average m/z.
     */
    public double getMZ(){
        double sum = 0;
        for(Identification iden:identifications){
            sum += iden.getMz();
        }
        return sum/identifications.size();
    }
    /**
     * Unimplemented as now only MS2 considered
     * @return The retention time.
     */
    public Double getRT(){
        throw new UnsupportedOperationException("Unimplemented as now only MS2 considered.");
    }
    /**
     * Get the feature mzQuantML element
     * @return The feature.
     */
    public Feature convertToQfeature(){
        Feature feature = new Feature();
        feature.setId(getId());
        feature.setCharge(String.valueOf(charge));
        feature.setMz(getMZ());
        feature.setRt(String.valueOf(getRT()));
        return feature;
    }
    
    @Override
    public int getCount(){
        return identifications.size();
    }
}
