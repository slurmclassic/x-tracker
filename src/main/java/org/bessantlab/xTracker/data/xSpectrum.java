package org.bessantlab.xTracker.data;

/**
 * The interface of spectrum
 * @author Jun Fan
 */
public interface xSpectrum {
    public double[] getMzData(String filename);
    public double[] getIntensityData(String filenames);
}
