package org.bessantlab.xTracker.plugins.rawdataLoad;

import org.bessantlab.xTracker.plugins.pluginInterface;
import org.bessantlab.xTracker.xTracker;

public abstract class rawData_loadPlugin implements pluginInterface{
    /**
     * Gets the plugin type.
     * @return plugin type
     */
    @Override
    public String getType(){
        return xTracker.RAWDATA_LOAD_TYPE;
    }
}
