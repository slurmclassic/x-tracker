package org.bessantlab.xTracker.plugins.identificationLoad;

import org.bessantlab.xTracker.plugins.pluginInterface;
import org.bessantlab.xTracker.xTracker;

public abstract class identData_loadPlugin implements pluginInterface{
    /**
     * Gets the plugin type.
     * @return plugin type
     */
    @Override
    public String getType(){
        return xTracker.IDENTIFICATION_LOAD_TYPE;
    }
    /**
     * create a new metadata object, populate it and assign to study
     */
    abstract void populateMetadata();
}
