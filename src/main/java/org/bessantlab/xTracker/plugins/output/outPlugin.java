package org.bessantlab.xTracker.plugins.output;

import org.bessantlab.xTracker.Utils;
import org.bessantlab.xTracker.plugins.pluginInterface;
import org.bessantlab.xTracker.utils.XMLparser;
import org.bessantlab.xTracker.xTracker;

public abstract class outPlugin implements pluginInterface{
    /**
     * Gets the plugin type.
     * @return plugin type
     */
    @Override
    public String getType(){
        return xTracker.OUTPUT_TYPE;
    }
    
    protected String getOutputFileName(String filename){
        return getOutputFileName(filename, "output", "outputFilename");
    }
    
    protected String getOutputFileName(String filename, String baseTag, String contentTag){
        XMLparser parser = new XMLparser(filename);
        parser.validate(baseTag);
        String outfile = parser.getElementContent(baseTag, contentTag);
        String path = Utils.getPath(outfile);
        return Utils.locateFile(path, xTracker.folders)+"/"+Utils.getFilename(outfile);  
//        return parser.getElementContent(baseTag, contentTag);
    }
    
}


